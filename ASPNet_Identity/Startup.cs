﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(ASPNet_Identity.Startup))]
namespace ASPNet_Identity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
